﻿-- Function: public.ksp_number_of_trees_in_plot(integer)

-- DROP FUNCTION public.ksp_number_of_trees_in_plot(integer);

CREATE OR REPLACE FUNCTION public.ksp_number_of_trees_in_plot(plot integer)
  RETURNS bigint AS
$BODY$
DECLARE
total_trees bigint;
BEGIN
	select count(obs_id) into total_trees
	from tree_obs
	where obs_id = plot and (vita_id = 2 or vita_id = 3);

	RETURN total_trees;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.ksp_number_of_trees_in_plot(integer)
  OWNER TO postgres;