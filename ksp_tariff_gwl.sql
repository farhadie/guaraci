-- Function: public.ksp_tariff_gwl(integer, integer, integer, character varying, integer, integer)

-- DROP FUNCTION public.ksp_tariff_gwl(integer, integer, integer, character varying, integer, integer);

CREATE OR REPLACE FUNCTION public.ksp_tariff_gwl(region integer, acidity integer, geology integer, exposition character varying, relief integer, elevation integer)
  RETURNS double precision AS
$BODY$DECLARE fc_tmp double precision;
DECLARE gwl double precision;
BEGIN
	SELECT k/(elevation-c)+m+(p*elevation) INTO fc_tmp
	FROM factor_combination INNER JOIN total_increment ON (factor_combination.fc = total_increment.fc)
	WHERE  region_id = region AND acidity_id = acidity AND (exposition_id = exposition) AND (geology_id = geology OR (geology_id IS NULL AND geology IS NULL)) AND relief_id = relief;

	return fc_tmp;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.ksp_tariff_gwl(integer, integer, integer, character varying, integer, integer)
  OWNER TO postgres;
