﻿-- Function: public.ksp_tariff_bifurcation_of_stem()

-- DROP FUNCTION public.ksp_tariff_bifurcation_of_stem();

CREATE OR REPLACE FUNCTION public.ksp_tariff_bifurcation_of_stem()
  RETURNS integer AS
$BODY$
BEGIN
	IF random() <0.0995 THEN
		RETURN 1;
	ELSE
		RETURN 0;
	END IF;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.ksp_tariff_bifurcation_of_stem()
  OWNER TO postgres;