--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.12
-- Dumped by pg_dump version 9.3.12
-- Started on 2016-05-19 09:00:15 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 351 (class 1259 OID 34110)
-- Name: total_increment; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE total_increment (
    id integer NOT NULL,
    fc integer,
    k double precision,
    m double precision,
    p double precision,
    c double precision,
    minhoe double precision,
    maxhoe double precision
);


--
-- TOC entry 350 (class 1259 OID 34108)
-- Name: total_increment_new_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE total_increment_new_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3815 (class 0 OID 0)
-- Dependencies: 350
-- Name: total_increment_new_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE total_increment_new_id_seq OWNED BY total_increment.id;


--
-- TOC entry 3641 (class 2604 OID 34113)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY total_increment ALTER COLUMN id SET DEFAULT nextval('total_increment_new_id_seq'::regclass);


--
-- TOC entry 3810 (class 0 OID 34110)
-- Dependencies: 351
-- Data for Name: total_increment; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (1, 1, 2042327.38800000004, 5074.68900000000031, 1.48859999999999992, 1873.90000000000009, 0, 1600);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (2, 2, 817233.633900000015, 6859.79100000000017, -1.12860000000000005, 1761.70000000000005, 0, 1600);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (3, 3, 0.0671999999999999958, 6148.47999999999956, -3.80080000000000018, 1600.00099999999998, 0, 1600);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (4, 4, 39685578736360704, 24050405171.848999, 14590.3106000000007, 1650100.10000000009, 118, 1600);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (5, 5, 18716565.1295000017, 9705.3739999999998, 3.56199999999999983, 2815.0010000000002, 0, 1600);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (6, 6, 744875972826732, 1241022014.93109989, 2075.28690000000006, 600210.099999999977, 610, 1600);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (7, 7, 1119175.46769999992, 5733.25600000000031, -0.339600000000000013, 2018.5, 0, 1800);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (8, 8, 625035427.46449995, 73368.7350000000006, 6.56099999999999994, 9137.95000000000073, 0, 1800);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (9, 9, 11581367.7084999997, 5341.35599999999977, 3.69090000000000007, 2984.59999999999991, 0, 2100);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (10, 10, 2420947686491376, 2250872009.37960005, 2096.74420000000009, 1075560, 0, 2100);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (11, 11, 5067627187855744, 3066645027.5999999, 1857.36699999999996, 1652500, 0, 2300);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (12, 12, 13026340700812160, 8273839084.34469986, 5264.79929999999968, 1574400.19999999995, 561, 2300);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (13, 13, 805295.599999999977, 4830.52400000000034, -0.841099999999999959, 2362.80000000000018, 0, 2100);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (14, 14, 899860.400000000023, 3291.19799999999987, 0.137199999999999989, 2351.40000000000009, 0, 2100);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (15, 15, 6199444.59999999963, 9294.70999999999913, -0.131399999999999989, 2484.40000000000009, 0, 1800);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (16, 16, 516638.200000000012, 5256.72800000000007, -0.0926000000000000018, 1901.5, 0, 1800);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (17, 17, 315005.099999999977, 4210.30400000000009, -0.491099999999999981, 1894.70000000000005, 0, 1800);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (18, 18, 30106.2999999999993, 3432.09700000000021, -0.43969999999999998, 1811.40000000000009, 0, 1800);
INSERT INTO total_increment (id, fc, k, m, p, c, minhoe, maxhoe) VALUES (19, 19, 19521.0999999999985, 2997.09999999999991, -0.670200000000000018, 1810.90000000000009, 0, 1800);


--
-- TOC entry 3816 (class 0 OID 0)
-- Dependencies: 350
-- Name: total_increment_new_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('total_increment_new_id_seq', 1, false);


-- Completed on 2016-05-19 09:00:15 CEST

--
-- PostgreSQL database dump complete
--

