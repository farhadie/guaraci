--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.12
-- Dumped by pg_dump version 9.3.12
-- Started on 2016-05-24 12:02:45 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 351 (class 1259 OID 34140)
-- Name: factor_combination; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE factor_combination (
    id integer NOT NULL,
    region_id integer,
    acidity_id integer,
    geology_id integer,
    exposition_id character varying(1),
    relief_id integer,
    fc integer
);


--
-- TOC entry 3810 (class 0 OID 34140)
-- Dependencies: 351
-- Data for Name: factor_combination; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (6, 1, 1, NULL, 'N', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (7, 1, 1, NULL, 'N', 2, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (8, 1, 1, NULL, 'N', 3, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (9, 1, 1, NULL, 'N', 4, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (10, 1, 1, NULL, 'N', 5, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (11, 1, 1, NULL, 'S', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (12, 1, 1, NULL, 'S', 2, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (13, 1, 1, NULL, 'S', 3, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (14, 1, 1, NULL, 'S', 4, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (15, 1, 1, NULL, 'S', 5, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (21, 1, 2, NULL, 'N', 1, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (22, 1, 2, NULL, 'N', 2, 1);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (23, 1, 2, NULL, 'N', 3, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (24, 1, 2, NULL, 'N', 4, 3);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (25, 1, 2, NULL, 'N', 5, 6);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (26, 1, 2, NULL, 'S', 1, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (28, 1, 2, NULL, 'S', 3, 5);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (27, 1, 2, NULL, 'S', 2, 4);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (29, 1, 2, NULL, 'S', 4, 6);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (30, 1, 2, NULL, 'S', 5, 6);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (1, 1, 1, NULL, '_', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (2, 1, 1, NULL, '_', 2, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (3, 1, 1, NULL, '_', 3, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (4, 1, 1, NULL, '_', 4, NULL);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (5, 1, 1, NULL, '_', 5, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (16, 1, 2, NULL, '_', 1, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (17, 1, 2, NULL, '_', 2, 1);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (18, 1, 2, NULL, '_', 3, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (19, 1, 2, NULL, '_', 4, NULL);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (20, 1, 2, NULL, '_', 5, 6);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (31, 1, 1, NULL, '', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (32, 1, 1, NULL, '', 2, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (33, 1, 1, NULL, '', 3, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (34, 1, 1, NULL, '', 4, NULL);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (35, 1, 1, NULL, '', 5, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (36, 1, 2, NULL, '', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (37, 1, 2, NULL, '', 2, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (38, 1, 2, NULL, '', 3, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (39, 1, 2, NULL, '', 4, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (40, 1, 2, NULL, '', 5, 8);

--To be according the basel dataset 
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (6, 1, 1, 1, 'N', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (7, 1, 1, 1, 'N', 2, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (8, 1, 1, 1, 'N', 3, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (9, 1, 1, 1, 'N', 4, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (10, 1, 1, 1, 'N', 5, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (11, 1, 1, 1, 'S', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (12, 1, 1, 1, 'S', 2, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (13, 1, 1, 1, 'S', 3, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (14, 1, 1, 1, 'S', 4, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (15, 1, 1, 1, 'S', 5, 8);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (21, 1, 2, 1, 'N', 1, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (22, 1, 2, 1, 'N', 2, 1);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (23, 1, 2, 1, 'N', 3, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (24, 1, 2, 1, 'N', 4, 3);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (25, 1, 2, 1, 'N', 5, 6);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (26, 1, 2, 1, 'S', 1, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (28, 1, 2, 1, 'S', 3, 5);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (27, 1, 2, 1, 'S', 2, 4);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (29, 1, 2, 1, 'S', 4, 6);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (30, 1, 2, 1, 'S', 5, 6);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (1, 1, 1, 1, '_', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (2, 1, 1, 1, '_', 2, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (3, 1, 1, 1, '_', 3, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (4, 1, 1, 1, '_', 4, NULL);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (5, 1, 1, 1, '_', 5, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (16, 1, 2, 1, '_', 1, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (17, 1, 2, 1, '_', 2, 1);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (18, 1, 2, 1, '_', 3, 2);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (19, 1, 2, 1, '_', 4, NULL);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (20, 1, 2, 1, '_', 5, 6);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (31, 1, 1, 1, '', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (32, 1, 1, 1, '', 2, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (33, 1, 1, 1, '', 3, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (34, 1, 1, 1, '', 4, NULL);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (35, 1, 1, 1, '', 5, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (36, 1, 2, 1, '', 1, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (37, 1, 2, 1, '', 2, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (38, 1, 2, 1, '', 3, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (39, 1, 2, 1, '', 4, 7);
INSERT INTO factor_combination (id, region_id, acidity_id, geology_id, exposition_id, relief_id, fc) VALUES (40, 1, 2, 1, '', 5, 8);


-- Completed on 2016-05-24 12:02:45 CEST

--
-- PostgreSQL database dump complete
--

