-- Function: public.ksp_tariff_sea_level(integer)

-- DROP FUNCTION public.ksp_tariff_sea_level(integer);

CREATE OR REPLACE FUNCTION public.ksp_tariff_sea_level(tree integer)
  RETURNS smallint AS
$BODY$
DECLARE
    sl SMALLINT;
BEGIN
SELECT sealevel INTO sl
FROM plot RIGHT JOIN plot_obs ON plot_obs.plot_id = plot.id
	  LEFT JOIN tree_obs ON tree_obs.obs_id = plot_obs.id
WHERE tree = tree_obs.id;
RETURN sl;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.ksp_tariff_sea_level(integer)
  OWNER TO postgres;
