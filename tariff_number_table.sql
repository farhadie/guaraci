--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.12
-- Dumped by pg_dump version 9.3.12
-- Started on 2016-05-24 11:26:02 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 352 (class 1259 OID 34122)
-- Name: tariff_number; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tariff_number (
    id integer NOT NULL,
    tariff_number integer,
    spec_id integer,
    region_id integer
);


--
-- TOC entry 3814 (class 0 OID 34122)
-- Dependencies: 352
-- Data for Name: tariff_number; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (1, 201, 16, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (2, 202, 16, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (3, 203, 16, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (4, 204, 16, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (5, 205, 16, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (6, 206, 15, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (7, 207, 15, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (8, 208, 16, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (9, 209, 15, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (10, 209, 15, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (11, 210, 17, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (12, 211, 17, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (13, 212, 17, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (14, 212, 17, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (15, 213, 18, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (16, 214, 18, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (17, 215, 19, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (18, 215, 19, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (19, 215, 19, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (20, 215, 20, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (21, 215, 20, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (22, 215, 20, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (23, 215, 21, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (24, 215, 21, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (25, 215, 21, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (26, 216, 1, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (27, 217, 1, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (28, 218, 1, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (29, 219, 1, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (30, 220, 1, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (31, 221, 4, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (32, 222, 4, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (33, 222, 4, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (34, 223, 2, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (35, 223, 3, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (36, 223, 24, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (37, 224, 2, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (38, 224, 3, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (39, 224, 24, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (40, 225, 5, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (41, 226, 5, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (42, 226, 5, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (43, 227, NULL, NULL);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (44, 228, 5, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (45, 228, 6, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (46, 228, 7, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (47, 228, 8, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (48, 228, 9, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (49, 228, 10, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (50, 228, 11, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (51, 228, 12, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (52, 228, 13, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (53, 228, 14, 1);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (54, 228, 5, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (55, 229, 6, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (56, 229, 7, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (57, 229, 8, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (58, 229, 9, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (59, 229, 10, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (60, 229, 11, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (61, 229, 12, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (62, 229, 13, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (63, 229, 14, 3);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (64, 230, 5, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (65, 230, 6, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (66, 230, 7, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (67, 230, 8, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (68, 230, 9, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (69, 230, 10, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (70, 230, 11, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (71, 230, 12, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (72, 230, 13, 4);
INSERT INTO tariff_number (id, tariff_number, spec_id, region_id) VALUES (73, 230, 14, 4);


-- Completed on 2016-05-24 11:26:02 CEST

--
-- PostgreSQL database dump complete
--

