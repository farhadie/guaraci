﻿-- Function: public.ksp_tariff_storey(integer)

-- DROP FUNCTION public.ksp_tariff_storey(integer);

CREATE OR REPLACE FUNCTION public.ksp_tariff_storey(tree integer)
  RETURNS integer AS
$BODY$DECLARE
	storey integer;
BEGIN
	SELECT rank_id INto storey
	FROM tree_obs
	WHERE tree_obs.id = tree;

	IF storey < 3 THEN
	RETURN 1;
	ELSE
	RETURN 0;
	END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.ksp_tariff_storey(integer)
  OWNER TO postgres;
