--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.12
-- Dumped by pg_dump version 9.3.12
-- Started on 2016-05-10 23:39:12 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 349 (class 1259 OID 33928)
-- Name: numbers; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE numbers (
    number integer NOT NULL
);


--
-- TOC entry 3797 (class 0 OID 33928)
-- Dependencies: 349
-- Data for Name: numbers; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO numbers VALUES (1);
INSERT INTO numbers VALUES (2);
INSERT INTO numbers VALUES (3);
INSERT INTO numbers VALUES (4);
INSERT INTO numbers VALUES (5);
INSERT INTO numbers VALUES (6);
INSERT INTO numbers VALUES (7);
INSERT INTO numbers VALUES (8);
INSERT INTO numbers VALUES (9);
INSERT INTO numbers VALUES (10);
INSERT INTO numbers VALUES (11);
INSERT INTO numbers VALUES (12);
INSERT INTO numbers VALUES (13);
INSERT INTO numbers VALUES (14);
INSERT INTO numbers VALUES (15);
INSERT INTO numbers VALUES (16);
INSERT INTO numbers VALUES (17);
INSERT INTO numbers VALUES (18);
INSERT INTO numbers VALUES (19);
INSERT INTO numbers VALUES (20);
INSERT INTO numbers VALUES (21);
INSERT INTO numbers VALUES (22);
INSERT INTO numbers VALUES (23);
INSERT INTO numbers VALUES (24);
INSERT INTO numbers VALUES (25);
INSERT INTO numbers VALUES (26);
INSERT INTO numbers VALUES (27);
INSERT INTO numbers VALUES (28);
INSERT INTO numbers VALUES (29);
INSERT INTO numbers VALUES (30);
INSERT INTO numbers VALUES (31);
INSERT INTO numbers VALUES (32);
INSERT INTO numbers VALUES (33);
INSERT INTO numbers VALUES (34);
INSERT INTO numbers VALUES (35);
INSERT INTO numbers VALUES (36);
INSERT INTO numbers VALUES (37);
INSERT INTO numbers VALUES (38);
INSERT INTO numbers VALUES (39);
INSERT INTO numbers VALUES (40);
INSERT INTO numbers VALUES (41);
INSERT INTO numbers VALUES (42);
INSERT INTO numbers VALUES (43);
INSERT INTO numbers VALUES (44);
INSERT INTO numbers VALUES (45);
INSERT INTO numbers VALUES (46);
INSERT INTO numbers VALUES (47);
INSERT INTO numbers VALUES (48);
INSERT INTO numbers VALUES (49);
INSERT INTO numbers VALUES (50);
INSERT INTO numbers VALUES (51);
INSERT INTO numbers VALUES (52);
INSERT INTO numbers VALUES (53);
INSERT INTO numbers VALUES (54);
INSERT INTO numbers VALUES (55);
INSERT INTO numbers VALUES (56);
INSERT INTO numbers VALUES (57);
INSERT INTO numbers VALUES (58);
INSERT INTO numbers VALUES (59);
INSERT INTO numbers VALUES (60);
INSERT INTO numbers VALUES (61);
INSERT INTO numbers VALUES (62);
INSERT INTO numbers VALUES (63);
INSERT INTO numbers VALUES (64);
INSERT INTO numbers VALUES (65);
INSERT INTO numbers VALUES (66);
INSERT INTO numbers VALUES (67);
INSERT INTO numbers VALUES (68);
INSERT INTO numbers VALUES (69);
INSERT INTO numbers VALUES (70);
INSERT INTO numbers VALUES (71);
INSERT INTO numbers VALUES (72);
INSERT INTO numbers VALUES (73);
INSERT INTO numbers VALUES (74);
INSERT INTO numbers VALUES (75);
INSERT INTO numbers VALUES (76);
INSERT INTO numbers VALUES (77);
INSERT INTO numbers VALUES (78);
INSERT INTO numbers VALUES (79);
INSERT INTO numbers VALUES (80);
INSERT INTO numbers VALUES (81);
INSERT INTO numbers VALUES (82);
INSERT INTO numbers VALUES (83);
INSERT INTO numbers VALUES (84);
INSERT INTO numbers VALUES (85);
INSERT INTO numbers VALUES (86);
INSERT INTO numbers VALUES (87);
INSERT INTO numbers VALUES (88);
INSERT INTO numbers VALUES (89);
INSERT INTO numbers VALUES (90);
INSERT INTO numbers VALUES (91);
INSERT INTO numbers VALUES (92);
INSERT INTO numbers VALUES (93);
INSERT INTO numbers VALUES (94);
INSERT INTO numbers VALUES (95);
INSERT INTO numbers VALUES (96);
INSERT INTO numbers VALUES (97);
INSERT INTO numbers VALUES (98);
INSERT INTO numbers VALUES (99);
INSERT INTO numbers VALUES (100);


--
-- TOC entry 3631 (class 2606 OID 33932)
-- Name: p; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY numbers
    ADD CONSTRAINT p PRIMARY KEY (number);


-- Completed on 2016-05-10 23:39:12 CEST

--
-- PostgreSQL database dump complete
--

