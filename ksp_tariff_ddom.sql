﻿-- Function: public.ksp_tariff_ddom(integer, numeric)

-- DROP FUNCTION public.ksp_tariff_ddom(integer, numeric);

CREATE OR REPLACE FUNCTION public.ksp_tariff_ddom(plot integer, edgef numeric)
  RETURNS numeric AS
$BODY$
DECLARE ddom numeric;
BEGIN
	WITH temp AS (
		SELECT tree_id, dbh, vita_id, number
		FROM tree_obs
			JOIN numbers
				ON (10000/(9.77^2)*3.14)*edgef >= Numbers.number
		WHERE obs_id = plot
		ORDER BY dbh DESC
	), largest_trees AS (
		SELECT * FROM temp LIMIT 100
	)
	SELECT AVG(dbh) into ddom
	FROM largest_trees;
	RETURN ddom;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.ksp_tariff_ddom(integer, numeric)
  OWNER TO postgres;
