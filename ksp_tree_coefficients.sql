﻿-- Function: public.ksp_tree_coefficients(integer, integer)

-- DROP FUNCTION public.ksp_tree_coefficients(integer, integer);

CREATE OR REPLACE FUNCTION public.ksp_tree_coefficients(region integer, spec integer)
  RETURNS nfi_coefficients AS
$BODY$
	SELECT nfi_coefficients.id, nfi_coefficients.tariff_number, b0,b1,b2,b3,b4,b5,b6,b7
	FROM nfi_coefficients 
		LEFT JOIN tariff_number ON tariff_number.tariff_number= nfi_coefficients.tariff_number
	WHERE region = tariff_number.region_id AND spec = tariff_number.spec_id;$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION public.ksp_tree_coefficients(integer, integer)
  OWNER TO postgres;
