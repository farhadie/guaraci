-- View: public.basis_volume

-- DROP VIEW public.basis_volume;

CREATE OR REPLACE VIEW public.basis_volume AS 
 SELECT tree_obs.id,
    tree_obs.tree_id,
    tree.spec_id,
    plot_obs.region_id,
    ksp_tariff_gwl(plot_obs.region_id, plot_obs.acidity_id, NULL::integer, plot.exposition, plot_obs.relief_id, plot.sealevel::integer) AS gwl,
    ksp_tariff_ddom(plot_obs.id, plot_obs.forest_edgef) AS ddom,
    ksp_tariff_bifurcation_of_stem() AS bifurcation_of_stem,
    ksp_tariff_sea_level(tree_obs.id) AS sea_level,
    ksp_tariff_storey(tree_obs.id) AS storey,
    ksp_nfi_tariff_function(tree_obs.id) AS volume
   FROM tree_obs
     JOIN tree ON tree.id = tree_obs.tree_id
     JOIN plot_obs ON plot_obs.id = tree_obs.obs_id
     JOIN plot ON plot_obs.plot_id = plot.id
  WHERE tree_obs.dbh > 0;

ALTER TABLE public.basis_volume
  OWNER TO postgres;

