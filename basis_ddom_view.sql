﻿-- View: public.basis_ddom

-- DROP VIEW public.basis_ddom;

CREATE OR REPLACE VIEW public.basis_ddom AS 
 SELECT plot_obs.plot_id,
    ksp_tariff_ddom(plot_obs.plot_id, plot_obs.forest_edgef) AS ddom
   FROM plot_obs
  ORDER BY plot_obs.plot_id;

ALTER TABLE public.basis_ddom
  OWNER TO postgres;
