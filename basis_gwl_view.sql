-- View: public.basis_gwl

-- DROP VIEW public.basis_gwl;

CREATE OR REPLACE VIEW public.basis_gwl AS 
 SELECT plot.id,
    plot_obs.plot_id,
    plot_obs.year,
    plot_obs.region_id,
    plot_obs.acidity_id,
    plot_obs.geology_id,
    plot.exposition,
    plot_obs.relief_id,
    plot.sealevel,
    ksp_tariff_gwl(plot_obs.region_id, plot_obs.acidity_id, plot_obs.geology_id, plot.exposition, plot_obs.relief_id, plot.sealevel::integer) AS gwl
   FROM plot_obs
     FULL JOIN plot ON plot.id = plot_obs.plot_id;

ALTER TABLE public.basis_gwl
  OWNER TO postgres;

