﻿-- Function: public.ksp_nfi_tariff_function(integer)

-- DROP FUNCTION public.ksp_nfi_tariff_function(integer);

CREATE OR REPLACE FUNCTION public.ksp_nfi_tariff_function(input_tree integer)
  RETURNS numeric AS
$BODY$DECLARE
	spec integer;
	b0 numeric;
	b1 numeric;
	b2 numeric;
	b3 numeric;
	b4 numeric;
	b5 numeric;
	b6 numeric;
	b7 numeric;
	region integer;
	volume numeric;
BEGIN
	select tree.spec_id, region_id into spec, region
	from tree_obs join tree on tree.id = tree_obs.tree_id join plot_obs on tree_obs.obs_id = plot_obs.id
	where tree_obs.id = input_tree;
	
	SELECT ksp_tree_coefficients.b0,
		ksp_tree_coefficients.b1,
		ksp_tree_coefficients.b2,
		ksp_tree_coefficients.b3,
		ksp_tree_coefficients.b4,
		ksp_tree_coefficients.b5,
		ksp_tree_coefficients.b6,
		ksp_tree_coefficients.b7
	INTO b0,b1,b2,b3,b4,b5,b6,b7
	FROM ksp_tree_coefficients(region, spec);
	
	SELECT exp(b0 + b1 *ln(dbh) + b2 * (ln(dbh)^4) + b3 * ksp_tariff_gwl(plot_obs.region_id,plot_obs.acidity_id, null, plot.exposition, plot_obs.relief_id, plot.sealevel)+ b4 * ksp_tariff_ddom(plot.id, plot_obs.forest_edgef)+ b5 * ksp_tariff_bifurcation_of_stem()+ b6 * ksp_tariff_sea_level(tree_obs.id)+ b7 * ksp_tariff_storey(tree_obs.id)) INTO volume
	from tree_obs join plot_obs on tree_obs.obs_id = plot_obs.id 
			join plot on plot_obs.plot_id = plot.id
	where tree_obs.id = input_tree;
	RETURN volume;
END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.ksp_nfi_tariff_function(integer)
  OWNER TO postgres;
